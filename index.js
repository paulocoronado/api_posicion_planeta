import express from 'express';
import {computeEclipticCoordinates, keplerianElements2050} from "./scripts/planetPosition.js"
 

const app=express();

app.get("/saludo", ()=>{console.log("hola Mundo!!")});

app.get('/posicion', (peticion, respuesta)=>{

    

    let planeta= Number(peticion.query.planeta);
    let tiempo= Date.parse(peticion.query.tiempo);

    if(isNaN(tiempo)){
        tiempo = new Date().getTime();
        //tiempo=new Date(1997, 6, 21, 0, 0, 0, 0);
    }

    if(isNaN(planeta)){
        planeta=4;
    }

    console.log(tiempo);

    let posicion= computeEclipticCoordinates(keplerianElements2050, planeta, tiempo);
    console.log(posicion);
    let miJson=JSON.stringify(posicion);
    console.log(miJson);
    respuesta.send(miJson);
    return true;
}
);

const port=3000;
 
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
app.listen(port, ()=>{console.log(`Server running in port ${port}`)})